<?php

defined('MOODLE_INTERNAL') || die;

$name = 'theme_dynamo/logo';
$title = get_string('logo','theme_dynamo');
$description = get_string('logodesc', 'theme_dynamo');
$setting = new admin_setting_configtext($name, $title, $description, 'http://conradtweb.com/files/images/logo2.png', PARAM_URL);
$settings->add($setting);

$name = 'theme_dynamo/linkcolor';
$title = get_string('linkcolor','theme_dynamo');
$description = get_string('linkcolordesc', 'theme_dynamo');
$default = '#84a9d3';
$setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null);
$settings->add($setting);

$name = 'theme_dynamo/navhovercolor';
$title = get_string('navhovercolor','theme_dynamo');
$description = get_string('navhovercolordesc', 'theme_dynamo');
$default = '#84a9d3';
$setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null);
$settings->add($setting);

//colourpicker