<?php

function dynamo_set_linkcolor($css, $linkcolor) {
    $tag = '[[setting:linkcolor]]';
    $replacement = $linkcolor;
    if (is_null($replacement)) {
        $replacement = '#84a9d3';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function dynamo_set_navhovercolor($css, $linkcolor) {
    $tag = '[[setting:navhovercolor]]';
    $replacement = $linkcolor;
    if (is_null($replacement)) {
        $replacement = '#84a9d3';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function dynamo_set_logo($css, $logoURL) {
    $tag = '[[setting:logo]]';
    $replacement = $logoURL;
    if (is_null($replacement)) {
        $replacement = 'http://conradtweb.com/files/images/logo2.png';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function dynamo_process_css($css, $theme)
{
	if (!empty($theme->settings->linkcolor)) {
        $linkcolor = $theme->settings->linkcolor;
    } else {
        $linkcolor = null;
    }
    $css = dynamo_set_linkcolor($css, $linkcolor);

    if (!empty($theme->settings->navhovercolor)) {
        $navhovercolor = $theme->settings->navhovercolor;
    } else {
        $navhovercolor = null;
    }
    $css = dynamo_set_navhovercolor($css, $navhovercolor);

    if (!empty($theme->settings->logo)) {
        $logo = $theme->settings->logo;
    } else {
        $logo = null;
    }
    $css = dynamo_set_logo($css, $logo);

	return $css;
}